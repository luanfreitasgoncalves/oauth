<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/tasks', 'TasksController@getAll')->name('getAllTasks');
        Route::post('/tasks', 'TasksController@add')->name('addTasks');
        Route::get('/tasks/{id}', 'TasksController@get')->name('getTasks');
        Route::post('/tasks/{id}', 'TasksController@edit')->name('editTasks');
        Route::post('/tasks/delete/{id}', 'TasksController@delete')->name('deleteTasks');
});
Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => 'client-id',
        'redirect_uri' => 'http://127.0.0.1/callback',
        'response_type' => 'code',
        'scope' => '',
    ]);

    return redirect('http://127.0.0.1/oauth/authorize?'.$query);
});
